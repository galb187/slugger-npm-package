export function slugger(...args){
    return args.map(str => str.split(' ').join('-')).join('-')
}

