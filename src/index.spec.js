import { slugger} from "./index.js";

/**
 * @describe [optional] - group of tests with a header to describe them
 */
 describe('testing slugger basic functionality', () => {
    /**
     * @it - unit tests can use the 'it' syntax
     */
    it('slugger can slug string with spaces', () => {
        expect("slugger-can-slug-string-with-spaces")
        .toEqual(slugger("slugger can slug string with spaces"));
    })
    /**
     * @test - unit test can use the 'test' syntax
     */
    test('slugger can slug any number of spacy strings', () => {
        expect("slugger-can-slug-any-number-of-spacy-strings")
        .toEqual(slugger("slugger can slug any","number of spacy strings"));
    })
})